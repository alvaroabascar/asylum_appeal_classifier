
.PHONY: data_dirs copy_to_server download_labelled_data

#################################################################################
# GLOBALS                                                                       #
#################################################################################

PYTHON_INTERPRETER = python3.7

#################################################################################
# COMMANDS                                                                      #
#################################################################################

requirements:
	pip install -r requirements.txt

data_dirs:
	mkdir -p data/{raw,interim,processed}

classification_upload_tasks:
	gzip data/interim/data_to_classify.jsonl
	scp data/interim/data_to_classify.jsonl.gz prodigy_irene:~/
	gzip -d data/interim/data_to_classify.jsonl
	ssh prodigy_irene "gzip -d -f data_to_classify.jsonl.gz"

ner_upload_tasks:
	gzip data/interim/dataset_ner.jsonl
	scp data/interim/dataset_ner.jsonl.gz prodigy_irene:~/ner/
	gzip -d data/interim/dataset_ner.jsonl.gz
	ssh prodigy_irene "gzip -d -f ner/dataset_ner.jsonl.gz"

classification_download_results:
	ssh prodigy_irene "bash db_out.sh"
	scp prodigy_irene:~/data_classified.jsonl data/interim/data_classified.jsonl

ner_download_results:
	ssh prodigy_irene "cd ner && bash db_out.sh && gzip ner_annotated.jsonl"
	scp prodigy_irene:~/ner/ner_annotated.jsonl.gz data/interim/ner_annotated.jsonl.gz
