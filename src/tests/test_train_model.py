import pytest
import spacy

from ..models.train_model import evaluate

class FakeTextCat:
    def __init__(self, labels, cats):
        self.cats = cats
        self.dicts_labels = self.labels_to_dicts(labels)
        
    def pipe(self, docs):
        for doc, labels in zip(docs, self.dicts_labels):
            doc.cats = labels
        return docs

    def labels_to_dicts(self, labels):
        dicts_labels = []
        for label in labels:
            dic = {cat: label == cat for cat in self.cats}
            dicts_labels.append(dic)
        return dicts_labels
        
def test_evaluate():
    nlp = spacy.blank('es')
    tk = nlp.create_pipe('tokenizer')

    correct_cats = ['rejected', 'approved', 'approved', 'rejected',
                    'return_to_trial', 'rejected']

    not_correct_cats = ['approved', 'approved', 'approved', 'rejected',
                        'return_to_trial', 'rejected']    
    
    cats = {'rejected', 'approved', 'return_to_trial'}
    textcat = FakeTextCat(correct_cats,
                          cats)
    texts = ['blablah' + str(i) for i, _ in enumerate(correct_cats)]
    metrics = evaluate(tk, textcat, texts, textcat.dicts_labels)
    assert metrics['global']['precision'] == pytest.approx(1.0)
    assert metrics['global']['recall'] == pytest.approx(1.0)

    textcat2 = FakeTextCat(not_correct_cats,
                           cats)

    metrics = evaluate(tk,
                       textcat2,
                       texts,
                       textcat2.labels_to_dicts(correct_cats))
    assert metrics['approved']['precision'] == pytest.approx(2/3)
    assert metrics['approved']['recall'] == pytest.approx(1.0)
    assert metrics['rejected']['precision'] == pytest.approx(1.0)
    assert metrics['rejected']['recall'] == pytest.approx(2/3)
    assert metrics['return_to_trial']['precision'] == pytest.approx(1.0)
    assert metrics['return_to_trial']['recall'] == pytest.approx(1.0)
    
    
