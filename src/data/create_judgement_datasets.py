"""
Script to split judgements into "dev", "train", "validation" datasets.
The script creates a new table "datasets" in the database, and a many-to-many
table judgements_to_datasets which links judgements to datasets.
"""
import random
import sqlite3

import click
import srsly

TRAIN_ID = 0
VALIDATION_ID = 1
TEST_ID = 2


@click.command()
@click.argument('database_path', type=click.File('r'))
@click.option('--n-test', type=int, default=400)
@click.option('--n-train', type=int, default=2000)
@click.option('--n-valid', type=int, default=400)
@click.option('--deterministic-splits', type=click.File('r'))
def main(database_path, n_test, n_train, n_valid, deterministic_splits):
    """
    If deterministic_splits is specified, it is a json with entries train,
    test, valid. Each entry contains a list of examples which should belong
    to the given split. Each example is identified by the reference_number
    and case_url.
    """
    random.seed(42)
    connection = sqlite3.connect(database_path.name)
    cursor = connection.cursor()

    # create tables datasets and judgements_to_datasets
    cursor.execute('DROP TABLE IF EXISTS datasets')
    cursor.execute('DROP TABLE IF EXISTS judgements_to_datasets')    
    cursor.execute('''
CREATE TABLE datasets (
    dataset_id INT PRIMARY KEY,
    name TEXT,
    description TEXT
)
''')
    cursor.execute('''
CREATE TABLE judgements_to_datasets (
    judgement_id INT,
    dataset_id INT,
    FOREIGN KEY(judgement_id) REFERENCES judgements(judgement_id),
    FOREIGN KEY(dataset_id) REFERENCES datasets(dataset_id)
)
''')

    # populate datasets
    datasets_data = [
        [TRAIN_ID, "train", "Texts used to train models or develop rules."],
        [VALIDATION_ID, "validation", "Texts used to check performance metrics during "
         "development"],
        [TEST_ID, "test", "Texts used to obtain final metrics."]
    ]
    cursor.executemany('INSERT INTO datasets VALUES (?,?,?)', datasets_data)

    # sample judgements, fill the three datasets, ensuring they are disjoint
    cursor.execute('SELECT judgement_id, reference_number, case_url FROM judgements')
    examples = cursor.fetchall()
    example_to_id = {(e[1], e[2]): e[0] for e in examples}
    
    ids_train = set()
    ids_test = set()
    ids_valid = set()

    if deterministic_splits:
        splits = srsly.read_json(deterministic_splits.name)
        # populate ids_train, ids_test, ids_valid with the deterministic splits
        for partition_set, partition_name in [(ids_train, 'train'),
                                              (ids_test, 'test'),
                                              (ids_valid, 'valid')]:
            
            for example in splits[partition_name]:
                example_id = example_to_id[(example['reference_number'],
                                            example['case_url'])]
                partition_set.add(example_id)
        
    used_ids = ids_train.union(ids_test).union(ids_valid)
    judgement_ids = set(example_to_id.values()).difference(used_ids)
    ids_train.update(random.sample(judgement_ids, n_train - len(ids_train)))
    ids_test.update(random.sample(judgement_ids.difference(ids_train),
                                  n_test - len(ids_test)))
    ids_valid.update(random.sample(
        judgement_ids.difference(
            ids_train.union(ids_test)
        ),
        n_valid - len(ids_valid)))

    print('len ids_train:', len(ids_train))
    print('len ids_valid:', len(ids_valid))
    print('len ids_test:', len(ids_test))        

    cursor.executemany(
        ('INSERT INTO judgements_to_datasets (dataset_id, judgement_id) '
         'VALUES (?,?)'),
        [[TRAIN_ID, judgement_id] for judgement_id in ids_train])
    cursor.executemany(
        ('INSERT INTO judgements_to_datasets (dataset_id, judgement_id) '
         'VALUES (?,?)'),
        [[VALIDATION_ID, judgement_id] for judgement_id in ids_valid])    
    cursor.executemany(
        ('INSERT INTO judgements_to_datasets (dataset_id, judgement_id) '
         'VALUES (?,?)'),
        [[TEST_ID, judgement_id] for judgement_id in ids_test])

    connection.commit()
    connection.close()


if __name__ == '__main__':
    main()
