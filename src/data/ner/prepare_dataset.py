"""
Script to prepare a dataset to choose entities with prodigy (judge, country of
origin, etc.).
"""

import click
import srsly
import sqlite3

@click.command()
@click.argument('database_location', type=click.File('r'))
@click.argument('output_jsonl', type=click.File('w'))
@click.option('--n-test', type=int, default=50)
@click.option('--n-validation', type=int, default=50)
def main(database_location, output_jsonl, n_test, n_validation):
    conn = sqlite3.connect(database_location.name)
    cursor = conn.cursor()
    query_template = '''
SELECT j.judgement_id, j.text FROM judgements j
INNER JOIN judgements_to_datasets jd ON jd.judgement_id = j.judgement_id
INNER JOIN datasets d ON d.dataset_id = jd.dataset_id
WHERE d.name = '{}'
ORDER BY random()
LIMIT {}
'''
    judgements = []    
    cursor.execute(query_template.format('validation', n_validation))
    for row in cursor.fetchall():
        # append (id, text)
        judgements.append({'id': row[0],
                           'text': row[1],
                           'dataset': 'validation'})

    cursor.execute(query_template.format('test', n_test))
    for row in cursor.fetchall():
        # append (id, text)
        judgements.append({'id': row[0],
                           'text': row[1],
                           'dataset': 'test'})

    srsly.write_jsonl(output_jsonl.name, judgements)



if __name__ == '__main__':
    main()
