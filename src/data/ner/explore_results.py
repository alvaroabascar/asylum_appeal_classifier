"""
Simple script to explore NER annotations.
"""
from collections import Counter

import click
import srsly


@click.command()
@click.argument('data_path', type=click.File('r'))
@click.option('-n', '--n-words-context', type=int, default=2)
@click.option('-e', '--n-words-entities', type=int, default=2)
def main(data_path, n_words_context, n_words_entities):
    data = list(srsly.read_jsonl(data_path.name))
    # 'label': ["<contexts>"]
    contexts = {}
    entities = {}
    for example in data:
        spans = example['spans']
        for span in spans:
            label = span['label']
            if label not in contexts:
                contexts[label] = []
            if label not in entities:
                entities[label] = []
            ctx_start = max(span['start'] - 50, 0)
            ctx_end = span['start']
            ctx = example['text'][ctx_start:ctx_end]
            ctx = ' '.join(ctx.strip().split()[-n_words_context:])
            ent = example['text'][span['start']:span['end']]
            print('#' * 50)
            print('Label:', label)
            print('Context:', ctx + '[' + ent + ']')
            ent = ' '.join(ent.strip().split()[:n_words_entities])
            contexts[label].append(ctx.lower())
            entities[label].append(ent.lower())

    print('\n' * 3)
    print('######## Contexts #########')
    for label, ctxs in contexts.items():
        print('\n\nLabel:', label)
        for ctx, count in Counter(ctxs).most_common(20):
            print(ctx, count)

    print('\n\n######## Entities #########')
    for label, ents in entities.items():
        print('\n\nLabel:', label)
        for ent, count in Counter(ents).most_common(20):
            print(ent, count)


if __name__ == '__main__':
    main()
