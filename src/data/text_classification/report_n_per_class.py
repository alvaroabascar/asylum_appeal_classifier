"""
Report how many examples we have per class, taking into account the division
into train/test/validation.
"""
from sqlalchemy import create_engine

import click
import pandas as pd


@click.command()
@click.argument('database_path', type=click.File('r'))
@click.option('--export-csv', type=click.File('w'), default=None)
def main(database_path, export_csv):
    sql_engine = create_engine(f'sqlite:///{database_path.name}', echo=False)
    connection = sql_engine.raw_connection()
    query_n_labels = (
        'select l.label_name, d.name as dataset_name, count(*) as n '
        'from judgements j '
        'inner join judgements_to_labels jtl '
        'on jtl.judgement_id = j.judgement_id '
        'inner join labels l '
        'on l.label_id = jtl.label_id '
        'inner join judgements_to_datasets jtd '
        'on jtd.judgement_id = j.judgement_id '
        'inner join datasets d '
        'on d.dataset_id = jtd.dataset_id '
        'group by l.label_name, d.name '
        'order by count(*) '
        'desc')

    df = pd.read_sql_query(query_n_labels, connection).sort_values(by=['label_name', 'dataset_name'])
    print(df)
    if export_csv:
        df.to_csv(export_csv)


if __name__ == '__main__':
    main()
