"""
This script takes the annotated dataset exported from prodigy and saves the
annotations to the database.
"""

import sqlite3
from collections import Counter

import click
import srsly


@click.command()
@click.argument('database_path', type=click.File('r'))
@click.argument('prodigy_dataset', type=click.File('r'))
def main(database_path, prodigy_dataset):
    data = list(srsly.read_jsonl(prodigy_dataset.name))
    n_orig = len(data)
    # we modify our annotated dataset so that for binary classifications we
    # always have either one label or the other
    # if a text is not labelled TYPE B, we add the label TYPE A.
    # wrt language, we only label REVERSE_LANGUAGE for TYPE B. In case an
    # example IS TYPE B and is not REVERSE_LANGUAGE, we add the label
    # REGULAR_LANGUAGE
    all_label_combinations = []
    data = [d for d in data if d['answer'] == 'accept']
    print(f'Out of {n_orig} examples, {len(data)} are marked as "accept".')
    for example in data:
        add_labels = []
        if 'TYPE_B' not in example['accept']:
            add_labels.append('TYPE_A')
        if 'REVERSE_LANGUAGE' not in example['accept'] and 'TYPE_B' in example[
                'accept']:
            add_labels.append('REGULAR_LANGUAGE')
        example['accept'] += add_labels
        all_label_combinations.append(tuple(sorted(example['accept'])))

    print('\nFrequencies:\n')
    for item, count in Counter(all_label_combinations).most_common():
        print(item, count)

    connection = sqlite3.connect(database_path.name)
    cursor = connection.cursor()

    cursor.execute('DROP TABLE IF EXISTS labels')
    cursor.execute('DROP TABLE IF EXISTS judgements_to_labels')

    query = cursor.execute('SELECT judgement_id, reference_number, case_url '
                           'FROM judgements')

    ref_url_to_id = {}
    for _id, ref, url in query.fetchall():
        ref_url_to_id[(ref, url)] = _id

    # the 'accept' entry contains the labels added to each text
    labels = {l for d in data for l in d['accept']}
    # the 'answer' entry contains the status of the example: accept, reject,
    # ignore
    labels.update(d['answer'] for d in data)

    # create a table for labels
    cursor.execute('CREATE TABLE IF NOT EXISTS labels '
                   '(label_id INTEGER PRIMARY KEY AUTOINCREMENT,'
                   ' label_name TEXT)')

    # create a table to connect judgements to labels
    cursor.execute('CREATE TABLE IF NOT EXISTS judgements_to_labels '
                   '(judgement_id INT, label_id INT)')

    labels_in_db = [
        r[0]
        for r in cursor.execute('SELECT label_name FROM labels').fetchall()
    ]
    labels_to_add = labels.difference(labels_in_db)
    cursor.executemany('INSERT INTO labels (label_name) VALUES (?)',
                       [[label] for label in labels_to_add])
    query = cursor.execute('SELECT label_id, label_name FROM labels')
    label_to_id = {r[1]: r[0] for r in query.fetchall()}

    judgements_to_labels = []
    for example in data:
        labels = [example['answer']] + example['accept']
        judgement_id = ref_url_to_id[(example['reference_number'],
                                      example['case_url'])]
        for label in labels:
            label_id = label_to_id[label]
            judgements_to_labels.append([judgement_id, label_id])

    cursor.executemany(
        'INSERT INTO judgements_to_labels '
        '(judgement_id, label_id) '
        'VALUES (?, ?)', judgements_to_labels)

    connection.commit()
    connection.close()


if __name__ == '__main__':
    main()
