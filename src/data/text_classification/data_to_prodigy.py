# -*- coding: utf-8 -*-
"""
Create a dataset for prodigy, without labels, to be manually labeled.
"""
import logging
import random
import sqlite3
from pathlib import Path

import click
import spacy
import srsly
import tqdm
from prodigy import set_hashes
from sklearn.model_selection import train_test_split


def subsample_dataset(dataset, n):
    """Subsample stratified per year, preserving the proportion of cases
    from different years.

    Args:
        dataset (list of dict): list of examples.
        n (int): number of examples to send to prodigy.

    Returns:
        subsampled dataset (list of dict) where len(dataset) = n, and the
          dataset keeps the sampe proportion of examples per year.

    """
    years = [e['promulgation_date'].split('-')[0] for e in dataset]
    subsample, _ = train_test_split(dataset, train_size=n, stratify=years)
    return subsample


@click.command()
@click.argument('input_database', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
@click.option('--n-train', type=int)
@click.option('--n-test', type=int)
@click.option('--n-valid', type=int)
@click.option('--ignore-examples', type=click.File('r'))
def main(input_database, output_filepath, n_train, n_test, n_valid,
         ignore_examples):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    random.seed(42)
    logger = logging.getLogger(__name__)
    logger.info('making final data set from raw data')

    conn = sqlite3.connect(input_database)
    cursor = conn.cursor()

    cols = [
        'judgement_id', 'reference_number', 'text', 'case_url',
        'promulgation_date', 'hearing_date', 'last_updated_on',
        'bailii_identifier', 'appellant_name', 'case_title', 'country',
        'judges', 'reported'
    ]
    cols_list = ', '.join(f'j.{col}' for col in cols)

    # we are filtering those after 15 february 2010 because
    # before there, there is no Type B, and at this moment
    # we want to focus on the rest.
    query_template = (f'SELECT {cols_list} '
                      'FROM judgements j '
                      'INNER JOIN judgements_to_datasets jtd '
                      'ON jtd.judgement_id = j.judgement_id '
                      'INNER JOIN datasets d '
                      'ON d.dataset_id = jtd.dataset_id '
                      'WHERE d.name = \'{dataset}\' '
                      'AND promulgation_date >= \'2010-03-01\' '
                      'ORDER BY random() '
                      'LIMIT {n}')

    dataset = []
    for dataset_name, n in [('train', n_train), ('test', n_test),
                            ('validation', n_valid)]:
        query = query_template.format(dataset=dataset_name, n=n)
        cursor.execute(query)
        dataset.extend([dict(zip(cols, row)) for row in cursor.fetchall()])

    # if ignore_examples is set, it is a dict with splits of examples which
    # must be ignored. Each example is defined by reference_number and case_url
    if ignore_examples:
        ignore = srsly.read_json(ignore_examples.name)
        ignore = ignore['train'] + ignore['test'] + ignore['valid']
        ignore = {(ex['reference_number'], ex['case_url']) for ex in ignore}
        dataset = [
            ex for ex in dataset
            if (ex['reference_number'], ex['case_url']) not in ignore
        ]

    options = [{
        'id': 'APPROVED',
        'text': 'Approved'
    }, {
        'id': 'DISMISSED',
        'text': 'Dismissed'
    }, {
        'id': 'RETURN_TO_TRIAL',
        'text': 'Return to First-tier Tribunal'
    }, {
        'id': 'TYPE_B',
        'text': 'Type B (Appeal by Home Office)'
    }, {
        'id': 'REVERSE_LANGUAGE',
        'text': 'Language refers to original ftt appeal'
    }]
    nlp = spacy.blank('en')
    nlp.max_length = 2000000
    tasks = []
    n_missing_text = 0
    for i, example in enumerate(tqdm.tqdm(dataset)):
        if not example['text']:
            n_missing_text += 1
            continue
        doc = nlp(example['text'])
        # we colour some spans to facilitate text classification
        example['spans'] = [{
            "start": tk.idx,
            "end": tk.idx + len(tk),
            "label": tk.lemma_
        } for tk in doc if tk.lemma_ in {'dismiss', 'remit', 'allow'}]
        example['options'] = options
        example['label'] = example['reference_number']

        tasks.append(set_hashes(example))

    print('Number of missing texts:', n_missing_text)
    random.seed(42)
    random.shuffle(tasks)
    srsly.write_jsonl(output_filepath, tasks)


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]
    main()
