"""
This script is used to divide our prodigy results into train/test/valid. Once
we have this dataset divided, we will use it to alter our database sets so that
prodigy test results appear in database test, prodigy train in database train,
and prodigy valid in database valid.
"""

import random

import click
import srsly

@click.command()
@click.argument('prodigy_results', type=click.File('r'))
@click.argument('output_path', type=click.File('w'))
@click.option('--perc-train', type=float, required=True)
@click.option('--perc-test', type=float, required=True)
@click.option('--perc-valid', type=float, required=True)
def main(prodigy_results, output_path, perc_train, perc_test, perc_valid):
    data = list(srsly.read_jsonl(prodigy_results.name))
    n = len(data)
    random.seed(42)
    random.shuffle(data)
    split_train = int(perc_train * n)
    split_test = split_train + int(perc_test * n)
    data_train = data[:split_train]
    data_test = data[split_train:split_test]
    data_valid = data[split_test:]
    split = {
        'train': [{'reference_number': d['reference_number'],
                   'case_url': d['case_url']} for d in data_train],
        'test': [{'reference_number': d['reference_number'],
                   'case_url': d['case_url']} for d in data_test],
        'valid': [{'reference_number': d['reference_number'],
                   'case_url': d['case_url']} for d in data_valid]
    }
    srsly.write_json(output_path.name, split)


if __name__ == '__main__':
    main()
