import json
import string

import click
import sqlite3
import srsly
import tqdm
from sqlite3 import Error

def clean_case_name(link_name):
    return link_name.split(' (unreported)')[0]
    
 
@click.command()
@click.argument('bailii_json', type=click.File('r'))
def main(bailii_json):
    data = set()
    counts = 0    
    for line in tqdm.tqdm(bailii_json.readlines()):
        try:
            case = json.loads(line.strip(',' + string.whitespace))
        except Exception as e:
            print('shit')
            continue

        reference_number = clean_case_name(case['case_link_name'])
        data.add((reference_number, case['text']))
        counts += 1

    print(f'Number of cases: {counts}')
    print(f'Number of non-duplicated cases: {len(data)}')
            



if __name__ == '__main__':
    main()
