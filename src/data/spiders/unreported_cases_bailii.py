import datetime
import os
import re
import tempfile
import urllib
from functools import partial

import html2text
import requests
import scrapy
import textract
from dateparser.date_parser import DateParser

from helpers import parse_doc

USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36'


class UnreportedCases(scrapy.Spider):
    name = 'unreported_cases'
    start_urls = ['http://www.bailii.org/uk/cases/UKAITUR/']
    headers= {'User-Agent': ('Mozilla/5.0 (X11; Linux x86_64; rv:48.0) '
                             'Gecko/20100101 Firefox/48.0')}

    date_parser = DateParser()
    
    # from 1 June we have them in tribunaldecisions.service.gov.uk
    max_datetime = datetime.datetime(2013, 6, 1)
    
    def start_requests(self):

        for url in self.start_urls:
            yield scrapy.Request(url,
                                 headers=self.headers,
                                 callback=self.find_years)

    def parse_case(self, response, date, identifier, case_link_name):
        case_url = response.request.url
        selector_list = response.css('.WordSection1')
        error = None
        if selector_list:
            text = html2text.html2text(selector_list[0].extract())
            doc_url = None
        else:
            links = response.css('center a::attr(href)').extract()
            doc_rel_url = next(l for l in links if l.endswith('.doc'))
            doc_url = urllib.parse.urljoin(response.url, doc_rel_url)
            try:
                text = parse_doc(doc_url)
            except Exception as e:
                text = None
                error = str(e)

        yield {
            'identifier': identifier,
            'date': date,
            'text': text,
            'case_url': case_url,
            'doc_url': doc_url,
            'error': error,
            'case_link_name': case_link_name
        }

    def find_cases(self, response):
        for li in response.css('li'):
            texts = li.css('::text').getall()
            text = ''.join(texts)
            matches = re.findall('(\w+)[\s\S]+?\((\d+\s\w+\s\d+)\)', text)
            identifier, date = matches[0]

            page_href = li.css('a::attr(href)').get()
            case_link_name = li.css('a::text').get()
            doc_href = page_href.replace('.html', '.doc')
            doc_url = urllib.parse.urljoin(response.url, doc_href)
            
            if self.max_datetime:
                datetime, precision = self.date_parser.parse(date)
                if datetime > self.max_datetime:
                    continue

            parse_fn = partial(self.parse_case,
                               date=date,
                               identifier=identifier,
                               case_link_name=case_link_name)
            
            yield response.follow(page_href, parse_fn,
                                  headers=self.headers)
            
        
    def find_years(self, response):
        # desired_years = set(map(str, range(2003, 2014)))
        for page in response.css('blockquote > a'):
            text = page.css('::text').get()
            # if text not in desired_years:
            #     continue
            href = page.css('::attr(href)').get()
            yield response.follow(href, self.find_cases,
                                  headers=self.headers)
