import os
import tempfile

import requests
import textract
from striprtf.striprtf import rtf_to_text

try:
    from xml.etree.cElementTree import XML
except ImportError:
    from xml.etree.ElementTree import XML
import zipfile

USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36'

WORD_NAMESPACE = '{http://schemas.openxmlformats.org/wordprocessingml/2006/main}'
PARA = WORD_NAMESPACE + 'p'
TEXT = WORD_NAMESPACE + 't'


def get_docx_text(path):
    """
    Take the path of a docx file as argument, return the text in unicode.
    """
    document = zipfile.ZipFile(path)
    xml_content = document.read('word/document.xml')
    document.close()
    tree = XML(xml_content)

    paragraphs = []
    for paragraph in tree.getiterator(PARA):
        texts = [node.text
                 for node in paragraph.getiterator(TEXT)
                 if node.text]
        if texts:
            paragraphs.append(''.join(texts))

    return '\n\n'.join(paragraphs)



def parse_doc(doc_url):
    headers = {
        'User-Agent': USER_AGENT
    }
    r = requests.get(doc_url, headers=headers, stream=True)
    if r.status_code != 200:
        raise Exception(str(r.status_code))
    
    fname = tempfile.mktemp() + '.doc'
    
    with open(fname, 'wb') as fh:
        for chunk in r:
            fh.write(chunk)

    try:
        text = textract.process(fh.name).decode('utf-8')
    except Exception as e:
        if 'rich text format' in str(e):
            with open(fh.name):
                content = fh.read()
            text = rtf_to_text(content)
        elif 'zip' in str(e).lower():
            text = get_docx_text(fh.name)
        else:
            raise e
    finally:
        os.remove(fname)
    return text
