import datetime
import os
import re
import tempfile
import urllib
from functools import partial

import html2text
import requests
import scrapy
import textract
from dateparser.date_parser import DateParser

from helpers import parse_doc

USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36'


class UnreportedCases(scrapy.Spider):
    name = 'unreported_reported_cases_tribunaldecisions'
    start_urls = ['https://tribunalsdecisions.service.gov.uk/utiac']
    headers= {'User-Agent': ('Mozilla/5.0 (X11; Linux x86_64; rv:48.0) '
                             'Gecko/20100101 Firefox/48.0')}

    def start_requests(self):
        for url in self.start_urls:
            yield scrapy.Request(url,
                                 headers=self.headers,
                                 callback=self.find_pages)
        
    def find_pages(self, response):

        links = response.css('.pagination > ul > li > a::attr(href)').extract()
        page_nums = []
        for link in links:
            matches = re.findall('page=(\d+)', link)
            if matches:
                page_nums.append(int(matches[0]))
        last_page = max(page_nums)
        urls = []
        for i in range(1, last_page + 1):
            url = urllib.parse.urljoin(response.request.url, f'/utiac?&page={i}')
            yield response.follow(url, self.find_cases,
                                  headers=self.headers)

    def find_cases(self, response):

        tr_selector_list = response.css('.decisions-table > tr')
        for tr_selector in tr_selector_list[1:]:
            td_selector_list = tr_selector.css('td')
            case_url = td_selector_list[0].css('a::attr(href)').extract_first()
            if case_url is None:
                continue
            yield response.follow(case_url, self.parse_case, headers=self.headers)

    def parse_case(self, response):
        reference_number = response.css('.page-header > h1::text').extract_first()
        dic = {'reference_number': reference_number}
        for li in response.css('.decision-details > li'):
            detail_name_, detail_value_ = li.css('span') 
            detail_name = detail_name_.css('::text').extract_first().strip(':\n ')
            if detail_value_.css('time'): 
                detail_value = detail_value_.css(
                    'time::attr(timedate)').extract_first()
            else: 
                detail_value = detail_value_.css(
                    '::text').extract_first()
                if detail_value:
                    detail_value = detail_value.strip(':\n ')

            dic[detail_name] = detail_value

        html_decision = response.css('.decision-inner').extract_first()
        error = None
        if html_decision:
            text = html2text.html2text(html_decision)
        else:
            doc_url = response.css('.doc-file::attr(href)').extract_first()
            try:
                text = parse_doc(doc_url)
            except Exception as e:
                text = None
                error = str(e)

        dic['text'] = text
        dic['error'] = error
        dic['case_url'] = response.request.url
        yield dic
