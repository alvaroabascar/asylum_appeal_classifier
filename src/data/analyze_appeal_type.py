import os
import srsly

from .helpers import DATA_PATH


def secretary_is_respondent_appellant(text):
    text = text.lower()
    key = 'SECRETARY OF STATE FOR THE HOME DEPARTMENT'.lower()
    start_idx = text.find(key)
    end_idx = start_idx + len(key)
    ctx_after = text[end_idx + 15]
    has_respondent = 'respondent' in ctx_after
    has_appellant = 'appellant' in ctx_after
    return has_respondent, has_appellant

def run_check():
    data = srsly.read_json(os.path.join(DATA_PATH,
                                        'interim/fulldataset_with_docx.json'))
    n_no_texts = len([e for e in data if 'text' not in e])
    texts = [example['text'] for example in data if 'text' in example]
    results = [secretary_is_respondent_appellant(t) for t in texts]
    appellants = [i for i, [r, a] in enumerate(results) if a]
    respondents = [i for i, [r, a] in enumerate(results) if r]
    print(f'Number of appellants: {len(appellants)}')
    print(f'Number of respondents: {len(respondents)}')    
    
if __name__ == '__main__':
    run_check()
