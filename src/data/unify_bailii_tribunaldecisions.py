import datetime
import json
import string

import click
import srsly
import tqdm


def clean_case_name(link_name):
    return link_name.split(' (unreported)')[0]


def format_tribunaldecisions(tribunaldecisions_json):
    data = []
    # holds a count of reference numbers which are repeated
    reference_numbers = set()
    repeated_reference_numbers = set()
    for case in srsly.read_json(tribunaldecisions_json.name):

        reference_number = case['reference_number']

        if reference_number in reference_numbers:
            repeated_reference_numbers.add(reference_number)
        else:
            reference_numbers.add(reference_number)

        data.append({
            'reference_number': reference_number,
            'case_url': case['case_url'],
            'promulgation_date': case['Promulgation date'],
            'text': case['text'],
            'error': case['error'],
            # properties from tribunaldecisions
            'appellant_name': case['Appellant name'],
            'case_title': case['Case title'],
            'country': case['Country'],
            'hearing_date': case['Hearing date'],
            'judges': case['Judges'],
            'last_updated_on': case['Last updated on'],
            'publication_date': case['Publication date'],
            'status_of_case': case['Status of case']
        })

    for case in data:
        case['duplicated'] = (
            case['reference_number'] in repeated_reference_numbers)

    return data


def format_bailii(bailii_json):
    data = []
    # holds a count of reference numbers which are repeated
    reference_numbers = set()
    repeated_reference_numbers = set()
    for case in srsly.read_json(bailii_json.name):

        reference_number = clean_case_name(case['case_link_name'])

        if reference_number in reference_numbers:
            repeated_reference_numbers.add(reference_number)
        else:
            reference_numbers.add(reference_number)

        formatted_date = datetime.datetime.strptime(
            case['date'], '%d %B %Y').strftime('%Y-%m-%d')

        data.append({
            'reference_number': reference_number,
            'case_url': case['case_url'],
            'promulgation_date': formatted_date,
            'text': case['text'],
            'error': case['error'],
            'bailii_identifier': case['identifier'],
            # properties from tribunaldecisions
            'appellant_name': None,
            'case_title': None,
            'country': None,
            'hearing_date': None,
            'judges': None,
            'last_updated_on': None,
            'publication_date': None,
            'status_of_case': None
        })

    for case in data:
        case['duplicated'] = (
            case['reference_number'] in repeated_reference_numbers)

    return data


@click.command()
@click.argument('bailii_json', type=click.File('r'))
@click.argument('tribunaldecisions_json', type=click.File('r'))
@click.argument('output_json', type=click.File('w'))
def main(bailii_json, tribunaldecisions_json, output_json):
    data = format_bailii(bailii_json)
    tribunaldecisions_data = format_tribunaldecisions(tribunaldecisions_json)
    data.extend(tribunaldecisions_data)
    output_json.write(json.dumps(data))


if __name__ == '__main__':
    main()
