import os
import logging

import pandas as pd
import rpy2.robjects as robjects

from helpers import (
    DATA_PATH
)

LOGGER = logging.getLogger(__name__)

IN_FILE = os.path.join(DATA_PATH, 'raw/final1july.RData')
OUT_FILE = os.path.join(DATA_PATH, 'interim/final1july.csv.gz')


def clean_data(in_file, out_file):
    robjects.r['load'](in_file)
    data = robjects.r['tibble_1757_6039_1719_nodups_w29']
    colnames = data.colnames
    dic = {}
    for i, colname in enumerate(colnames):
        dic[colname] = list(data[i])
    df = pd.DataFrame(dic)
    df.set_index('doc_id')
    df.to_csv(out_file, index=False)


if __name__ == '__main__':
    clean_data(IN_FILE, OUT_FILE)
