import os
import re
from collections import Counter

import click
import srsly

@click.command()
@click.argument('dataset_path', type=click.File('r'))
def main(dataset_path):
    data = srsly.read_json(dataset_path.name)
    print('keys:\n-', "\n- ".join(sorted(data[0].keys())))
    print('\n\nNumber of cases:', len(data))
    print('Number of errors:', sum(1 for d in data if d['error']))
    years = [d['promulgation_date'].split('-')[0] for d in data]
    counts = Counter(years)
    print(counts)
    # print('Errors:\n-', "\n- ".join({d['error'] for d in data if d['error']}))

if __name__ == '__main__':
    main()
