"""
This script tries to find the origin of the appellant in texts, using either the
country name or the demonym. To do this it uses a list of countries and demonyms
and tries to do a simple lookup in the text.
"""
import os
import sqlite3
import re

import click
import pandas as pd

COUNTRY_MAPPINGS = {
    'China, Republic of': ['Taiwan'],
    'Virgin Islands, United States': ['United States Virgin Islands'],
    'Virgin Islands, British': ['British Virgin Islands'],
    'Saint Helena, Ascension and Tristan da Cunha':
    ['Saint Helena, Ascension and Tristan da Cunha'],
    'Korea, Democratic People\'s Republic of':
    ['Democratic People\'s Republic of Korea'],
    'Korea, Republic of': ['Republic of Korea'],
    'Eswatini (Swaziland)': ['Eswatini', 'Swaziland']
}


def split_row(string):
    # replace NO-BREAK SPACE by regular space
    string = string.replace(u'\xa0', u' ')
    return re.split(r',|/|\sor\s', string)


def extend_demonyms(demonyms):
    rules = [
        (r'men$', 'man'),
        (r's$', ''),
    ]
    new_demonyms = set()
    for demonym in demonyms:
        for rule in rules:
            replaced = re.sub(*rule, demonym)
            if replaced != demonym:
                new_demonyms.add(replaced)

    return list(new_demonyms.difference(demonyms))


def parse_countries(df):
    """
    Given a dataframe containing countries, adjectivals and demonyms, produce a
    dictionary with {country: {adjectivals: [], demonyms: []}}.
    """
    countries_keywords = {}
    for i, row in df.iterrows():
        country = row['Country/entity name']
        if country in COUNTRY_MAPPINGS:
            country = COUNTRY_MAPPINGS[country]
        elif ',' in country:
            name, extension = country.split(', ')
            country = [name, f'{extension} {name}']
        else:
            country = [country]
        adjectivals = [d.strip() for d in split_row(row['Adjectivals'])]
        demonyms = [d.strip() for d in split_row(row['Demonyms'])]
        new_demonyms = extend_demonyms(demonyms)
        demonyms.extend(new_demonyms)
        if 'a' in country or 'a' in adjectivals or 'a' in demonyms:
            print('Found "a" in ', row)
            print('name:', country)
            print('demonyms:', demonyms)
            print('adjectivals:', adjectivals)

        countries_keywords[i] = {
            'original_name': row['Country/entity name'],
            'names': country,
            'adjectivals': adjectivals,
            'demonyms': demonyms
        }
    return countries_keywords


@click.command()
@click.argument('database_file', type=click.File('r'))
@click.argument('countries_csv', type=click.File('r'))
def main(database_file, countries_csv):
    conn = sqlite3.connect(database_file.name)
    cursor = conn.cursor()
    df = pd.read_csv(countries_csv.name, index_col=None)
    kws = parse_countries(df)

    keyword_types = {'demonym': 1, 'country_name': 2, 'adjectival': 3}

    # create a list of lists [id, original country name]
    original_names = [[country_id, kws[country_id]['original_name']]
                      for country_id in kws.keys()]

    # create a list of lists [id, country keyword]. More than one row
    # can have the same id
    names = [[country_id, keyword, keyword_types['country_name']]
             for country_id in kws.keys()
             for keyword in kws[country_id]['names']]

    # create a list of lists [id, adjectival]. More than one row
    # can have the same id
    adjectivals = [[country_id, keyword, keyword_types['adjectival']]
                   for country_id in kws.keys()
                   for keyword in kws[country_id]['adjectivals']]

    # create a list of lists [id, demonym]. More than one row
    # can have the same id
    demonyms = [[country_id, keyword, keyword_types['demonym']]
                for country_id in kws.keys()
                for keyword in kws[country_id]['demonyms']]

    all_keywords = names + adjectivals + demonyms
    keywords = []
    seen = set()
    for country_id, keyword, keyword_type in all_keywords:
        if (country_id, keyword) not in seen:
            keywords.append([country_id, keyword, keyword_type])
            # print('adding', country_id, keyword)
            seen.add((country_id, keyword))

    conn.execute('DROP TABLE IF EXISTS countries')
    conn.execute('DROP TABLE IF EXISTS country_keywords')
    conn.execute('DROP TABLE IF EXISTS keyword_types')
    conn.execute('DROP VIEW IF EXISTS ambiguous_keywords')

    conn.execute('''
CREATE TABLE countries
(
    country_id INTEGER PRIMARY KEY,
    original_name TEXT
)''')

    conn.execute('''
CREATE TABLE keyword_types
(
    keyword_type_id INTEGER PRIMARY KEY,
    name TEXT
)''')

    conn.execute('''
CREATE TABLE country_keywords
(
    keyword_id INTEGER PRIMARY KEY,
    country_id INTEGER,
    keyword TEXT,
    keyword_type_id INTEGER
)''')

    conn.executemany('INSERT INTO countries VALUES (?,?)', original_names)
    conn.executemany(
        'INSERT INTO keyword_types (name, keyword_type_id) VALUES (?,?)',
        keyword_types.items())
    conn.executemany(
        'INSERT INTO country_keywords (country_id, keyword, keyword_type_id) VALUES (?,?,?)',
        keywords)

    conn.execute('''
CREATE VIEW ambiguous_keywords AS
WITH ambiguous_keywords AS (SELECT DISTINCT k1.keyword FROM country_keywords k1 INNER JOIN country_keywords k2 ON k1.keyword = k2.keyword AND k1.country_id != k2.country_id)
SELECT DISTINCT k.keyword_id, k.keyword_type_id, k.keyword, c.original_name AS original_country_name FROM country_keywords k
  INNER JOIN ambiguous_keywords ak ON ak.keyword = k.keyword
  INNER JOIN countries c ON c.country_id = k.country_id
ORDER BY k.keyword, k.country_id;
''')

    conn.commit()
    conn.close()


if __name__ == '__main__':
    main()
