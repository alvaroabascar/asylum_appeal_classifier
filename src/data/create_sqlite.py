"""
Script to create a SQLite database which will hold the appeal information.
"""

import os
import sqlite3
import sys

import click
import srsly
from tqdm import tqdm


SCHEMA_STATEMENT = '''
CREATE TABLE judgements
    -- Table holding all judgements, including texts, labels, etc.
(
    judgement_id INTEGER PRIMARY KEY,  -- unique id
    reference_number TEXT,   -- Identifiers the case, e.g. "jameel & AA-07505-2005"
    text TEXT,               -- Text written by the judge
    case_url TEXT,           -- URL to this case
    promulgation_date DATE,  -- 
    hearing_date DATE,       --
    last_updated_on DATE,    --
    bailii_identifier TEXT,  -- Identifier under bailii
    appellant_name TEXT,     -- Name of appellant
    case_title TEXT,         -- E.g. "YL (Nationality, Statelessness, Eritrea)"
    country TEXT,            -- Country of the appellant
    judges TEXT,             -- Judges involved in the trial
    reported BOOL,           -- True if Reported, False if Unreported
    duplicated BOOL          -- Whether this record seems duplicated, according
                             -- reference_number (https://gitlab.com/alvaroabascar/asylum_appeal_classifier/-/issues/11)
)
    '''


@click.command()
@click.argument('sqlite_path', type=click.File('w'))
@click.argument('data_path', type=click.File('r'))
@click.option('-f', '--force-overwrite', type=bool, default=False)
def main(sqlite_path: str, data_path: str, force_overwrite: bool):
    """
    Create the database.

    Args:
        sqlite_path (str): path to the database file.
        data_path (str): path to data to fill database.
        force_overwrite (bool): whether to overwrite file in case it exists.

    Returns:
        None

    """
    # check file existence, maybe remove it.
    if os.path.isfile(sqlite_path.name):
        if not force_overwrite:
            raise Exception(f"File '{sqlite_path}' already exists.")
        else:
            os.remove(sqlite_path.name)

    # read data
    raw_data = srsly.read_json(data_path.name)
    data = []

    fields = ['reference_number',
              'text',
              'case_url',
              'promulgation_date',
              'hearing_date',
              'last_updated_on',
              'bailii_identifier',
              'appellant_name',
              'case_title',
              'country',
              'judges',
              'reported',
              'duplicated']

    for example in tqdm(raw_data):
        if 'bailii_identifier' not in example:
            # cases from tribunaldecisions
            example['reported'] = example['status_of_case'].lower() == 'reported'
            example['bailii_identifier'] = None
        else:
            # cases from bailii
            example['reported'] = False

        row_data = tuple(example[field] for field in fields)
        data.append(row_data)

    conn = sqlite3.connect(sqlite_path.name)
    cursor = conn.cursor()

    # Create table
    cursor.execute(SCHEMA_STATEMENT)
    # Insert data
    fields_ = ','.join(f'"{field}"' for field in fields)
    placeholders = ','.join('?' for _ in fields)
    statement = f'INSERT INTO judgements ({fields_}) VALUES ({placeholders})'
    print(statement)
    cursor.executemany(statement, data)
        
    conn.commit()
    conn.close()


if __name__ == '__main__':
    main()
