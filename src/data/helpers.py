import os

THIS_FILE_PATH = os.path.abspath(os.path.dirname(__file__))
DATA_PATH = os.path.join(THIS_FILE_PATH, '../../data/')
