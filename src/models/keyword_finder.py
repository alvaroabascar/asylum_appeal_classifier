"""
This module uses a set of keywords to find matches in texts. Keywords and
texts are found in a sqlite3 database, and the user must indicate which
table of keywords are to be used.
"""

import sqlite3

import click
import spacy
from spacy.matcher import PhraseMatcher

def retrieve_table_data(cursor, table_name):
    """
    Given a table name, retrieve its data and return it as a list of dicts,
    where each dict represents a row, with format {col1: value1, col2: value2}.
    """
    cursor.execute(f'SELECT * FROM {table_name}')
    cols = [description[0] for description in cursor.description]
    data = []
    for row in cursor.fetchall():
        data.append(dict(zip(cols, row)))
    return data


@click.command()
@click.argument('database_path', type=str)
@click.argument('keyword_table_name', type=str)
def main(database_path, keyword_table_name):
    conn = sqlite3.connect(database_path)
    cursor = conn.cursor()
    keyword_data = retrieve_table_data(cursor, keyword_table_name)
    judgement_data = retrieve_table_data(cursor, 'judgements')    
    nlp = spacy.blank('en')
    matcher = PhraseMatcher(nlp.vocab)

    def on_match(matcher, doc, id, matches):
        for str_hash, token_start, token_end in matches:
            print(nlp.vocab.strings[str_hash], doc[token_start:token_end])
    
    for item in keyword_data:
        identifier = f'{keyword_table_name}_{item["keyword_id"]}'
        matcher.add(identifier, on_match, nlp(item["keyword"]))

    for judgement in judgement_data:
        print(judgement['text'])
        doc = matcher(nlp(judgement['text']))

if __name__ == '__main__':
    main()
