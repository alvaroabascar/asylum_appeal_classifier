from simpletransformers.classification import ClassificationModel
import pandas as pd
import logging


def train_simpletransformer(dataset: dict):
    all_labels = {
        label
        for split in dataset for e in dataset[split]
        for label in e['label_names']
    }
    labels_to_ints = {l: i for i, l in enumerate(sorted(all_labels))}

    # configure logging
    logging.basicConfig(level=logging.INFO)
    transformers_logger = logging.getLogger("transformers")
    transformers_logger.setLevel(logging.WARNING)

    train_data = [[e['text'][:500], labels_to_ints[e['label_names'][0]]]
                  for e in dataset['train']]
    train_df = pd.DataFrame(train_data)
    test_data = [[e['text'][:500], labels_to_ints[e['label_names'][0]]]
                 for e in dataset['validation']]
    eval_df = pd.DataFrame(test_data)

    # Create a ClassificationModel
    model = ClassificationModel('roberta', 'roberta-base', use_cuda=False)

    # Train the model
    model.train_model(
        train_df, auto_weights=True, multi_label=(len(all_labels) > 2))

    # Evaluate the model
    result, model_outputs, wrong_predictions = model.eval_model(eval_df)

    print('Result:', result)
    # print('Model outputs:', model_outputs)
    # print('Wrong predictions:', len(wrong_predictions)/eval_df.shape[0])
