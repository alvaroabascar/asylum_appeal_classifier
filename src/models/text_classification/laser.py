# from imblearn.over_sampling import RandomOverSampler
from imblearn.under_sampling import RandomUnderSampler
from laserembeddings import Laser
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report
from sklearn.neural_network import MLPClassifier


def laser_encode(texts):
    laser = Laser()
    embeddings = laser.embed_sentences(texts, lang='en')
    return embeddings


def train_laser_rf(dataset: dict, sampler=None):
    texts_train = [ex['text'] for ex in dataset['train']]
    texts_valid = [ex['text'] for ex in dataset['validation']]

    embeddings_train = laser_encode(texts_train)
    embeddings_valid = laser_encode(texts_valid)

    labels_train = [ex['label_names'][0] for ex in dataset['train']]
    labels_valid = [ex['label_names'][0] for ex in dataset['validation']]

    if sampler:
        embeddings_train, labels_train = sampler.fit_resample(
            embeddings_train, labels_train)

    clf = RandomForestClassifier(n_estimators=1000, random_state=0)
    clf.fit(embeddings_train, labels_train)

    preds_valid = clf.predict(embeddings_valid)
    print(classification_report(labels_valid, preds_valid))
    return preds_valid


def train_laser_mlp(dataset: dict):
    texts_train = [ex['text'] for ex in dataset['train']]
    texts_valid = [ex['text'] for ex in dataset['validation']]

    labels_train = [ex['label_names'][0] for ex in dataset['train']]
    labels_valid = [ex['label_names'][0] for ex in dataset['validation']]

    embeddings_train = laser_encode(texts_train)
    embeddings_valid = laser_encode(texts_valid)

    ros = RandomUnderSampler(random_state=0)

    embeddings_train, labels_train = ros.fit_resample(embeddings_train,
                                                      labels_train)

    clf = MLPClassifier(hidden_layer_sizes=(256, ))
    clf.fit(embeddings_train, labels_train)

    preds_valid = clf.predict(embeddings_valid)
    print(classification_report(labels_valid, preds_valid))
    return preds_valid
