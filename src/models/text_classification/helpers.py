from imblearn.over_sampling import RandomOverSampler


def random_oversample(xs, ys):
    ros = RandomOverSampler(random_state=0)
    cats = [max(cats.items(), key=lambda x: x[1])[0] for cats in ys]
    cats_to_orig = dict(zip(cats, ys))
    new_xs, new_ys = ros.fit_resample([[x] for x in xs], cats)
    return [x[0] for x in new_xs], [cats_to_orig[y] for y in new_ys]
