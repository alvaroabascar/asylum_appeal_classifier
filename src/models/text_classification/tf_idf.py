from typing import List

from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import classification_report
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import SVC


def vectorize(dataset, texts):
    tf_idf_vectorizer = TfidfVectorizer(stop_words='english',
                                        ngram_range=(1, 4),
                                        lowercase=True,
                                        min_df=4)
    print(f'Building TF-IDF on {len(texts)} texts.')
    tf_idf_vectorizer.fit(texts)
    texts_train = [e['text'] for e in dataset['train']]
    labels_train = [e['label_names'][0] for e in dataset['train']]
    texts_valid = [e['text'] for e in dataset['validation']]
    labels_valid = [e['label_names'][0] for e in dataset['validation']]
    xs_train = tf_idf_vectorizer.transform(texts_train)
    xs_valid = tf_idf_vectorizer.transform(texts_valid)
    return xs_train, labels_train, xs_valid, labels_valid


def train_tfidf_nb(dataset: dict, texts: List[str], sampler):
    """
    Extract features using TF-IDF & then train a classifier (NB).
    """
    xs_train, labels_train, xs_valid, labels_valid = vectorize(dataset, texts)
    if sampler:
        xs_train, labels_train = sampler.fit_resample(xs_train, labels_train)
    clf = MultinomialNB()
    clf.fit(xs_train, labels_train)
    preds_valid = clf.predict(xs_valid)
    print(classification_report(labels_valid, preds_valid))
    return preds_valid


def train_tfidf_svc(dataset: dict, texts: List[str], sampler=None):
    """
    Extract features using TF-IDF & then train a classifier (NB).
    """
    xs_train, labels_train, xs_valid, labels_valid = vectorize(dataset, texts)
    if sampler:
        xs_train, labels_train = sampler.fit_resample(xs_train, labels_train)
    clf = SVC()
    clf.fit(xs_train, labels_train)
    preds_valid = clf.predict(xs_valid)
    print(classification_report(labels_valid, preds_valid))
    return preds_valid


def train_tfidf_rf(dataset: dict, texts: List[str], sampler=None):
    """
    Extract features using TF-IDF & then train a Random Forest.
    """
    xs_train, labels_train, xs_valid, labels_valid = vectorize(dataset, texts)
    if sampler:
        xs_train, labels_train = sampler.fit_resample(xs_train, labels_train)
    clf = RandomForestClassifier(n_estimators=1000, random_state=0)
    clf.fit(xs_train, labels_train)
    preds_valid = clf.predict(xs_valid)
    print(classification_report(labels_valid, preds_valid))
    return preds_valid
