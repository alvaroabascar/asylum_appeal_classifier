"""
Use a pretrained Albert model for text classification.
"""

import mlflow
import torch
from transformers import (AlbertForSequenceClassification, AlbertTokenizer,
                          Trainer, TrainingArguments)

from sklearn.metrics import accuracy_score, precision_recall_fscore_support


def compute_metrics(pred):
    labels = pred.label_ids
    preds = pred.predictions.argmax(-1)
    precision, recall, f1, _ = precision_recall_fscore_support(
        labels, preds, average='binary')
    acc = accuracy_score(labels, preds)
    return {
        'accuracy': acc,
        'f1': f1,
        'precision': precision,
        'recall': recall
    }


def batch_inference(model, tokenizer, id_to_label, texts, batch_size=10):
    all_preds = []
    for i in range(0, len(texts), batch_size):
        batch_texts = texts[i:i + batch_size]
        inputs = tokenizer(
            batch_texts, return_tensors="pt", padding=True, truncation=True)
        for key, val in inputs.items():
            inputs[key] = val.cuda()

        preds = model(**inputs)[0]
        preds = preds.argmax(axis=1)
        preds = [id_to_label[idx.item()] for idx in preds]
        all_preds.extend(preds)
    return all_preds


class AsylumDataset(torch.utils.data.Dataset):
    def __init__(self, encodings, labels):
        self.encodings = encodings
        self.labels = labels

    def __getitem__(self, idx):
        item = {
            key: torch.tensor(val[idx])
            for key, val in self.encodings.items()
        }
        item['labels'] = torch.tensor(self.labels[idx])
        return item

    def __len__(self):
        return len(self.labels)


def train_albert(dataset: dict, sampler):

    # dataset["train"] = dataset["train"][:10]
    texts_train = [e["text"] for e in dataset["train"]]
    texts_valid = [e["text"] for e in dataset["validation"]]
    labels_train = [e["label_names"][0] for e in dataset["train"]]
    labels_valid = [e["label_names"][0] for e in dataset["validation"]]

    # turn labels into ints
    labels = list(sorted(set(labels_train).union(labels_valid)))
    label_to_id = {label: i for i, label in enumerate(labels)}
    id_to_label = {i: label for label, i in label_to_id.items()}

    labels_train = list(map(label_to_id.get, labels_train))
    labels_valid = list(map(label_to_id.get, labels_valid))

    if sampler:
        texts_train, labels_train = sampler.fit_resample(
            [[t] for t in texts_train], labels_train)
        texts_train = [t[0] for t in texts_train]

    pretrained_model_name = "albert-base-v2"

    model = AlbertForSequenceClassification.from_pretrained(
        pretrained_model_name, num_labels=len(label_to_id))

    tokenizer = AlbertTokenizer.from_pretrained(pretrained_model_name)

    encodings_train = tokenizer(texts_train, truncation=True, padding=True)
    encodings_valid = tokenizer(texts_valid, truncation=True, padding=True)

    dataset_train = AsylumDataset(encodings_train, labels_train)
    dataset_valid = AsylumDataset(encodings_valid, labels_valid)

    training_args = TrainingArguments(
        output_dir='./results',
        num_train_epochs=30,
        per_device_train_batch_size=16,
        per_device_eval_batch_size=16,
        warmup_steps=500,
        weight_decay=0.01,
        logging_dir='./logs')
    # load_best_model_at_end=True,
    # metric_for_best_model="val_f1")

    trainer = Trainer(
        model=model,
        args=training_args,
        train_dataset=dataset_train,
        eval_dataset=dataset_valid)

    mlflow.end_run()
    trainer.train()

    # print('evaluation:', trainer.evaluate())

    preds, label_ids, metrics = trainer.predict(dataset_valid)
    pred_ids = preds.argmax(axis=1)
    return [id_to_label[id_] for id_ in pred_ids]
