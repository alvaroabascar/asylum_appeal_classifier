import math
import time
from collections import Counter

import mlflow
import numpy as np
import spacy
import torch
import torch.nn as nn
from sklearn.metrics import f1_score
from torch.utils.data import DataLoader, Dataset

DEVICE = "cuda" if torch.cuda.is_available() else "cpu"


class ClassificationDataset(Dataset):
    def __init__(self, docs, maxlen, labels, is_training):
        self.docs = docs
        self.labels = labels
        self.is_training = is_training
        self.maxlen = maxlen

    def __getitem__(self, index):
        doc = self.docs[index]
        label = self.labels[index]
        vecs = [tk.vector for tk in doc[:self.maxlen]]
        # add padding
        embed_dim = vecs[0].shape[0]
        pad = np.zeros(embed_dim) - 0.5
        for _ in range(self.maxlen - len(doc)):
            vecs.append(pad)
        vecs = torch.Tensor(vecs)
        # print('!!!', vecs.shape)
        if self.is_training:
            return {'text': vecs, 'label': label}
        else:
            return {'text': vecs}

    def __len__(self):
        return len(self.docs)


class TextClassifier(nn.Module):
    """
    Classifier Model.

    Args:
       embed_dim (int): size of embeddings.
       num_class (int): number of classes.
    """
    def __init__(self, params):
        super().__init__()
        # Parameters regarding text preprocessing
        self.seq_len = params["seq_len"]
        self.embedding_size = params["embedding_size"]

        # Dropout definition
        self.dropout = nn.Dropout(0.5)

        # CNN parameters definition
        # Kernel sizes
        self.kernel_1 = 2
        self.kernel_2 = 3
        self.kernel_3 = 4
        self.kernel_4 = 5

        # Output size for each convolution
        self.out_size = params["out_size"]

        # Number of strides for each convolution
        self.stride = params["stride"]

        # Convolution layers definition
        self.conv_1 = nn.Conv1d(self.seq_len, self.out_size, self.kernel_1,
                                self.stride)
        self.conv_2 = nn.Conv1d(self.seq_len, self.out_size, self.kernel_2,
                                self.stride)
        self.conv_3 = nn.Conv1d(self.seq_len, self.out_size, self.kernel_3,
                                self.stride)
        self.conv_4 = nn.Conv1d(self.seq_len, self.out_size, self.kernel_4,
                                self.stride)

        # Max pooling layers definition
        self.pool_1 = nn.MaxPool1d(self.kernel_1, self.stride)
        self.pool_2 = nn.MaxPool1d(self.kernel_2, self.stride)
        self.pool_3 = nn.MaxPool1d(self.kernel_3, self.stride)
        self.pool_4 = nn.MaxPool1d(self.kernel_4, self.stride)

        # Fully connected layer definition
        self.fc = nn.Linear(self.in_features_fc(), params["num_classes"])

    def in_features_fc(self):
        '''Calculates the number of output features after Convolution + Max pooling

          Convolved_Features = ((embedding_size + (2 * padding) -
                                 dilation * (kernel - 1) - 1) / stride) + 1
          Pooled_Features = ((embedding_size + (2 * padding) -
                              dilation * (kernel - 1) - 1) / stride) + 1

          source:
          https://pytorch.org/docs/stable/generated/torch.nn.Conv1d.html
          '''
        # Calcualte size of convolved/pooled features for
        # convolution_1/max_pooling_1 features
        out_conv_1 = ((self.embedding_size - 1 *
                       (self.kernel_1 - 1) - 1) / self.stride) + 1
        out_conv_1 = math.floor(out_conv_1)
        out_pool_1 = ((out_conv_1 - 1 *
                       (self.kernel_1 - 1) - 1) / self.stride) + 1
        out_pool_1 = math.floor(out_pool_1)

        # Calcualte size of convolved/pooled features for
        # convolution_2/max_pooling_2 features
        out_conv_2 = ((self.embedding_size - 1 *
                       (self.kernel_2 - 1) - 1) / self.stride) + 1
        out_conv_2 = math.floor(out_conv_2)
        out_pool_2 = ((out_conv_2 - 1 *
                       (self.kernel_2 - 1) - 1) / self.stride) + 1
        out_pool_2 = math.floor(out_pool_2)

        # Calcualte size of convolved/pooled features for
        # convolution_3/max_pooling_3 features
        out_conv_3 = ((self.embedding_size - 1 *
                       (self.kernel_3 - 1) - 1) / self.stride) + 1
        out_conv_3 = math.floor(out_conv_3)
        out_pool_3 = ((out_conv_3 - 1 *
                       (self.kernel_3 - 1) - 1) / self.stride) + 1
        out_pool_3 = math.floor(out_pool_3)

        # Calcualte size of convolved/pooled features for
        # convolution_4/max_pooling_4 features
        out_conv_4 = ((self.embedding_size - 1 *
                       (self.kernel_4 - 1) - 1) / self.stride) + 1
        out_conv_4 = math.floor(out_conv_4)
        out_pool_4 = ((out_conv_4 - 1 *
                       (self.kernel_4 - 1) - 1) / self.stride) + 1
        out_pool_4 = math.floor(out_pool_4)

        # Returns "flattened" vector (input for fully connected layer)
        return out_pool_1 * self.out_size
        # return (out_pool_1 + out_pool_2 + out_pool_3 +
        #         out_pool_4) * self.out_size

    def forward(self, x):

        # Convolution layer 1 is applied
        x1 = self.conv_1(x)
        x1 = torch.relu(x1)
        x1 = self.pool_1(x1)

        # Convolution layer 2 is applied
        x2 = self.conv_2(x)
        x2 = torch.relu((x2))
        x2 = self.pool_2(x2)

        # Convolution layer 3 is applied
        x3 = self.conv_3(x)
        x3 = torch.relu(x3)
        x3 = self.pool_3(x3)

        # Convolution layer 4 is applied
        x4 = self.conv_4(x)
        x4 = torch.relu(x4)
        x4 = self.pool_4(x4)

        # The output of each convolutional layer is concatenated into a
        # unique vector
        # union = torch.cat((x1, x2, x3, x4), 2)
        # union = union.reshape(union.size(0), -1)

        union = torch.cat((x1, ), 2)
        union = union.reshape(union.size(0), -1)

        # The "flattened" vector is passed through a fully connected layer
        out = self.fc(union)
        # Dropout is applied
        out = self.dropout(out)
        # Activation function is applied
        out = torch.sigmoid(out)

        return out.squeeze()


def evaluate(model, iterator, criterion):

    epoch_loss = 0
    epoch_acc = 0
    full_preds = []
    full_ys = []
    times = []

    model.eval()

    with torch.no_grad():

        for _, batch in enumerate(iterator):

            start = time.time()

            target = batch["label"].to(DEVICE)
            text = batch["text"].to(DEVICE)

            predictions = model(text)

            times.append(time.time() - start)

            loss = criterion(predictions, target)
            acc = accuracy(predictions, target)

            epoch_loss += loss.item()
            epoch_acc += acc.item()

            # retrieve the predictions for analysis
            full_preds += [p.item() for p in list(predictions.argmax(1))]
            full_ys += [p.item() for p in target]

        avg_time = sum(times) / float(len(times))

    return (epoch_loss / len(iterator), epoch_acc / len(iterator), full_preds,
            full_ys, avg_time)


def accuracy(preds, y):
    """
    Returns accuracy per batch
    """
    rounded_preds = preds.argmax(1)

    correct = (rounded_preds == y).float()  # convert into float for division
    acc = correct.sum() / len(correct)
    return acc


def train(model, iterator, optimizer, criterion):

    epoch_loss = 0
    epoch_acc = 0

    all_preds = []
    all_ys = []

    model.train()
    for _, batch in enumerate(iterator):

        optimizer.zero_grad()

        target = batch["label"].to(DEVICE)
        text = batch["text"].to(DEVICE)
        predictions = model(text)
        all_preds.extend([p.item() for p in list(predictions.argmax(1))])
        all_ys.extend([t.item() for t in target])

        loss = criterion(predictions, target)
        acc = accuracy(predictions, target)

        loss.backward()
        optimizer.step()

        epoch_loss += loss.item()
        epoch_acc += acc.item()

    f1 = f1_score(all_ys, all_preds)
    return epoch_loss / len(iterator), epoch_acc / len(iterator), f1


def train_torch_classifier(dataset):

    labels_train_str = [e['label_names'][0] for e in dataset['train']]
    labels_valid_str = [e['label_names'][0] for e in dataset['validation']]

    all_labels_str = set(labels_train_str)
    all_labels_str.update(labels_valid_str)

    label_to_int = {label: i for i, label in enumerate(all_labels_str)}
    int_to_label = {i: l for l, i in label_to_int.items()}

    nlp = spacy.load('en_core_web_lg')
    params = {
        "embedding_size": nlp.vocab.vectors.shape[1],
        "num_classes": len(label_to_int),
        "stride": 2,
        "seq_len": 5000,
        "out_size": 64,
        "epochs": 10,
        "batch_size": 20,
        "learning_rate": 0.01,
        "weight_decay": 0.0001
    }

    for param, value in params.items():
        mlflow.log_param(param, value)

    print('Processing texts with spaCy...', end='')
    with nlp.disable_pipes(['ner', 'tagger', 'parser']):
        docs_train = list(
            nlp.pipe(example['text'] for example in dataset['train']))
        docs_valid = list(
            nlp.pipe(example['text'] for example in dataset['validation']))

    print('[DONE]')
    max_len_train = max(map(len, docs_train))
    max_len_valid = max(map(len, docs_valid))
    mean_len_train = sum(map(len, docs_train)) / len(docs_train)
    mean_len_valid = sum(map(len, docs_valid)) / len(docs_valid)
    max_len = max(max_len_train, max_len_valid)
    print(f'Max num tokens: {max_len}')
    print(f'Mean len train: {mean_len_train}')
    print(f'Mean len valid: {mean_len_valid}')

    labels_train = [label_to_int[label] for label in labels_train_str]
    labels_valid = [label_to_int[label] for label in labels_valid_str]

    dataset_train = ClassificationDataset(docs_train,
                                          params["seq_len"],
                                          labels_train,
                                          is_training=True)
    dataset_valid = ClassificationDataset(docs_valid,
                                          params["seq_len"],
                                          labels_valid,
                                          is_training=True)

    loader_train = DataLoader(dataset_train,
                              batch_size=params["batch_size"],
                              shuffle=True)
    loader_valid = DataLoader(dataset_valid,
                              batch_size=params["batch_size"],
                              shuffle=False)

    class_counts = Counter(labels_train)
    class_weights = {}
    count_total = len(labels_train)
    for label, count in class_counts.items():
        class_weights[label] = count_total / count

    model = TextClassifier(params)
    optimizer = torch.optim.Adam(model.parameters(),
                                 lr=params["learning_rate"],
                                 weight_decay=params["weight_decay"])
    weights = torch.FloatTensor(
        [class_weights[c] for c in sorted(class_weights)]).to(DEVICE)
    criterion = nn.CrossEntropyLoss(weight=weights)

    for epoch in range(params["epochs"]):
        train_loss, train_acc, train_f1 = train(model, loader_train, optimizer,
                                                criterion)
        valid_loss, valid_acc, full_preds, ys, _ = evaluate(
            model, loader_valid, criterion)

        f1 = f1_score(ys, full_preds)

        mlflow.log_metric("loss", valid_loss)
        mlflow.log_metric("f1", f1)
        mlflow.log_metric("loss_train", train_loss)
        mlflow.log_metric("f1_train", train_f1)

        print(f'Epoch: {epoch+1:02} ')
        print(
            f'\tTrain Loss: {train_loss:.3f} | Train Acc: {train_acc*100:.2f}%'
        )
        print(
            f'\t Val. Loss: {valid_loss:.3f} |  Val. Acc: {valid_acc*100:.2f}%'
        )

    return [int_to_label[i] for i in full_preds]
