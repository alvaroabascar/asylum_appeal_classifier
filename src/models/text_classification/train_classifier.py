"""
Script to train a text classifier.
"""
# flake8: noqa C901
import os
import re
import shutil
import sqlite3
from collections import Counter
from typing import Dict, List, Tuple, Union

import click
import matplotlib.pyplot as plt
import mlflow
import pandas as pd
import seaborn as sn
from imblearn.over_sampling import RandomOverSampler
from imblearn.under_sampling import RandomUnderSampler
from sklearn.metrics import classification_report, confusion_matrix


def retrieve_dataset(
        connection: sqlite3.Connection,
        classes: List[str],
        text_range: Union[Tuple[int, int],
                          None] = None) -> Dict[str, List[dict]]:
    """
    Given a connection to the sqlite3 database and a list of classes, retrieves
    all cases which belogn to either of the given classes. Returns a dictionary
    with entires 'train', 'test', 'validation', whose values are a list with
    the cases corresponding to the given split.
    Args:
        connection (sqlite3.Connection): connection to sqtite3 db.
        classes (list(str)): list of labels of the texts to retrieve. Only
          texts with the provided labels will be retrieved.
        text_range (Optional(tuple(int, int))): part of the text to retrieve.
          If None, the full text is returned. If a tuple of (start, end) ints
          is provided, only text between characters "start" and "end" is
          returned.
    """
    dataset = {'train': [], 'test': [], 'validation': []}
    cursor = connection.cursor()
    classes_list = ', '.join(f"'{cls}'" for cls in classes)
    cols = [
        'j.judgement_id', 'j.text',
        'group_concat(l.label_name) as label_names', 'd.name as dataset_name'
    ]
    query = f'''
SELECT {', '.join(cols)}
FROM judgements j
INNER JOIN judgements_to_labels jtl
ON jtl.judgement_id = j.judgement_id
INNER JOIN judgements_to_datasets jtd
ON jtd.judgement_id = j.judgement_id
INNER JOIN labels l
ON l.label_id = jtl.label_id
INNER JOIN datasets d
ON d.dataset_id = jtd.dataset_id
WHERE l.label_name in ({classes_list})
GROUP BY j.judgement_id
'''

    result = cursor.execute(query)
    clean_cols = [re.sub(r'(?:.+\.|.+\sas\s)', '', col) for col in cols]
    for row in result.fetchall():
        example = dict(zip(clean_cols, row))
        example['label_names'] = example['label_names'].split(',')
        if text_range:
            example['text'] = example['text'][text_range[0]:text_range[1]]
            # print(example['label_names'])
            # print(example['text'])
            # print('#'*50)
            # print('\n'*4)
        dataset[example['dataset_name']].append(example)

    return dataset


def retrieve_train_texts(connection: sqlite3.Connection):
    query = """SELECT text FROM judgements j
INNER JOIN judgements_to_datasets jtd
  ON jtd.judgement_id = j.judgement_id
INNER JOIN datasets d
  ON jtd.dataset_id = d.dataset_id
WHERE d.name = 'train'
AND j.text IS NOT NULL
"""
    cursor = connection.cursor()
    cursor.execute(query)
    results = [r[0] for r in cursor.fetchall()]
    cursor.close()
    connection.close()
    return results


def print_report(dataset: Dict[str, List[dict]]) -> None:
    """
    Prints a report of n per class and split.
    """
    print('Counts of cases per category and split:\n')
    for split, examples in dataset.items():
        labels = [','.join(sorted(e['label_names'])) for e in examples]
        label_counts = Counter(labels)
        print(split, label_counts.most_common())


def merge_dataset_labels(dataset):
    for split, examples in dataset.items():
        to_pop = []
        for i, example in enumerate(examples):
            labels = example['label_names']
            set_labels = set(labels)
            if set_labels == {'TYPE_A', 'APPROVED', 'REGULAR_LANGUAGE'}:
                labels = ['FAVOR_INDIVIDUAL']
            elif set_labels == {'TYPE_A', 'DISMISSED', 'REGULAR_LANGUAGE'}:
                labels = ['FAVOR_HOME_OFFICE']
            elif set_labels == {'TYPE_B', 'APPROVED', 'REGULAR_LANGUAGE'}:
                labels = ['FAVOR_HOME_OFFICE']
            elif set_labels == {'TYPE_B', 'DISMISSED', 'REGULAR_LANGUAGE'}:
                labels = ['FAVOR_INDIVIDUAL']
            elif set_labels == {'TYPE_B', 'APPROVED', 'REVERSE_LANGUAGE'}:
                labels = ['FAVOR_INDIVIDUAL']
            elif set_labels == {'TYPE_B', 'DISMISSED', 'REVERSE_LANGUAGE'}:
                labels = ['FAVOR_HOME_OFFICE']
            elif 'RETURN_TO_TRIAL' in labels:
                labels = ['RETURN_TO_TRIAL']
            else:
                to_pop.append(i)
            example['label_names'] = labels
        for idx in to_pop[::-1]:
            examples.pop(idx)


@click.command()
@click.argument('database_path', type=click.File('r'))
@click.argument('model', type=str, default='spacy')
@click.option('--classes', type=str)
@click.option('--merge-labels/--no-merge-labels')
@click.option('--text-range', type=str, default=None)
@click.option('--test-as-train', type=bool, default=True)
@click.option('--balancing', type=str, default=None)
@click.option('-m', '--metrics-path', type=str, default=None)
def main(database_path, model, classes, merge_labels, text_range,
         test_as_train, balancing, metrics_path):
    connection = sqlite3.connect(database_path.name)
    if text_range:
        # should be a tuple (start_char, end_char)
        text_range = [int(pos) for pos in text_range.split(':')]
    if merge_labels:
        classes = [
            'REGULAR_LANGUAGE', 'TYPE_A', 'REVERSE_LANGUAGE', 'APPROVED',
            'TYPE_B', 'DISMISSED', 'RETURN_TO_TRIAL'
        ]
        dataset = retrieve_dataset(connection, classes, text_range)
        merge_dataset_labels(dataset)
    else:
        classes = classes.split(',')
        dataset = retrieve_dataset(connection, classes, text_range)

    if test_as_train:
        print(
            'Warning: merging "test" and "train" as a single train dataset.\n'
            'Evaluation is done on "validation" set.')
        dataset['train'].extend(dataset.pop('test'))

    texts = retrieve_train_texts(connection)
    print_report(dataset)

    params = {
        'model': model,
        'text_range': text_range,
        'text_as_train': test_as_train,
        'balancing': balancing,
        'n_train': len(dataset['train']),
        'n_valid': len(dataset['validation'])
    }
    mlflow.start_run(run_name='_'.join(classes))
    for name, val in params.items():
        mlflow.log_param(name, val)

    sampler = None
    if balancing == 'ros':
        sampler = RandomOverSampler(random_state=0)
    elif balancing == 'rus':
        sampler = RandomUnderSampler(random_state=0)
    elif balancing is None:
        sampler = None
    else:
        raise Exception('Balancing must be "ros" or "rus".')

    if model == 'spacy':
        from .spacy_classifier import train_spacy_classifier
        predictions = train_spacy_classifier(dataset, model='en_core_web_lg')
    elif model == 'simpletransformers':
        from .simpletransformers import train_simpletransformer
        train_simpletransformer(dataset)
    elif model == 'tf_idf_nb':
        from .tf_idf import train_tfidf_nb
        predictions = train_tfidf_nb(dataset, texts)
    elif model == 'tf_idf_svc':
        from .tf_idf import train_tfidf_svc
        predictions = train_tfidf_svc(dataset, texts)
    elif model == 'tf_idf_rf':
        from .tf_idf import train_tfidf_rf
        predictions = train_tfidf_rf(dataset, texts, sampler)
    elif model == 'torch':
        from .pytorch_clf import train_torch_classifier
        predictions = train_torch_classifier(dataset)
    elif model == 'laser_rf':
        from .laser import train_laser_rf
        predictions = train_laser_rf(dataset, sampler)
    elif model == 'laser_mlp':
        from .laser import train_laser_mlp
        predictions = train_laser_mlp(dataset)
    elif model == 'tars_zero':
        from .tars import predict_tars_zero
        predictions = predict_tars_zero(dataset)
    elif model == 'tars_train':
        from .tars import train_tars
        predictions = train_tars(dataset)
    elif model == 'albert':
        from .transformers import train_albert
        predictions = train_albert(dataset, sampler)
    else:
        raise Exception(f"Unknown model '{model}'")

    real_labels = [e['label_names'][0] for e in dataset['validation']]

    # for example, pred in zip(dataset['validation'], predictions):
    #     real_label = example['label_names'][0]
    #     if real_label != pred:
    #         print('Real label:', real_label)
    #         print('Predicted :', pred)
    #         print('Text:\n', example['text'])
    #         print('#' * 50)

    report = classification_report(real_labels, predictions)
    print(report)
    os.makedirs("outputs", exist_ok=True)
    with open("outputs/classification_report.txt", "w") as fh:
        for name, val in params.items():
            fh.write(f'- {name}: {val}\n')
        fh.write('\n')
        fh.write(report)

    shutil.copy("outputs/classification_report.txt", metrics_path)

    mlflow.log_artifacts("outputs")
    shutil.rmtree("outputs")

    mlflow.end_run()
    all_labels = set(real_labels).union(predictions)
    sorted_labels = sorted(all_labels)
    cm = confusion_matrix(real_labels, predictions, labels=sorted_labels)
    df_cm = pd.DataFrame(cm, index=sorted_labels, columns=sorted_labels)
    df_cm.columns.name = 'Predicted'
    df_cm.index.name = 'Real'
    sn.heatmap(df_cm, annot=True)
    plt.show()


if __name__ == '__main__':
    main()
