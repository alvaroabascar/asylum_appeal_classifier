"""
In this file we define different trainable models.
"""
import random
import shutil
import tempfile
from pathlib import Path

import mlflow
import spacy
from spacy.util import compounding, minibatch

from .helpers import random_oversample


def add_spacy_categories(dataset):

    all_labels = {
        label
        for split in dataset for example in dataset[split]
        for label in example['label_names']
    }

    for split, examples in dataset.items():
        for example in examples:
            # create a dictionary with the labels in the format expected by
            # spaCy
            cats = {}
            for label in example['label_names']:
                cats[label] = True
            for label in all_labels.difference(cats.keys()):
                cats[label] = False

            example['cats_spacy'] = cats

    return dataset, all_labels


def train_spacy_classifier(dataset: dict,
                           model: str = None,
                           output_dir: str = None,
                           n_iter: int = 15):

    tmp_dir = tempfile.mkdtemp()

    dataset, all_labels = add_spacy_categories(dataset)
    if output_dir is not None:
        output_dir = Path(output_dir)
        if not output_dir.exists():
            output_dir.mkdir()

    if model is not None:
        nlp = spacy.load(model)  # load existing spaCy model
        print("Loaded model '%s'" % model)
    else:
        nlp = spacy.blank("en")  # create blank Language class
        print("Created blank 'en' model")

    # add the text classifier to the pipeline if it doesn't exist
    # nlp.create_pipe works for built-ins that are registered with spaCy
    architecture = "bow"
    mlflow.log_param("architecture", architecture)
    textcat = nlp.create_pipe("textcat",
                              config={
                                  "exclusive_classes": True,
                                  "architecture": architecture
                              })
    nlp.add_pipe(textcat, last=True)

    # add label to text classifier
    for label in all_labels:
        textcat.add_label(label)

    train_texts = [e['text'] for e in dataset['train']]
    train_cats = [e['cats_spacy'] for e in dataset['train']]

    test_texts = [e['text'] for e in dataset['validation']]
    test_cats = [e['cats_spacy'] for e in dataset['validation']]

    train_texts, train_cats = random_oversample(train_texts, train_cats)

    print("Using ({} training, {} evaluation)".format(len(train_texts),
                                                      len(test_texts)))
    train_data = list(zip(train_texts, [{
        "cats": cats
    } for cats in train_cats]))

    # get names of other pipes to disable them during training
    pipe_exceptions = ["textcat", "trf_wordpiecer", "trf_tok2vec"]
    other_pipes = [
        pipe for pipe in nlp.pipe_names if pipe not in pipe_exceptions
    ]
    with nlp.disable_pipes(*other_pipes):  # only train textcat
        optimizer = nlp.begin_training()
        print("Training the model...")
        print("{:^5}\t{:^5}\t{:^5}\t{:^5}".format("LOSS", "P", "R", "F"))
        batch_sizes = compounding(4.0, 32.0, 1.001)
        best_f = 0
        for i in range(n_iter):
            losses = {}
            # batch up the examples using spaCy's minibatch
            random.shuffle(train_data)
            batches = minibatch(train_data, size=batch_sizes)
            for batch in batches:
                texts, annotations = zip(*batch)
                nlp.update(texts,
                           annotations,
                           sgd=optimizer,
                           drop=0.5,
                           losses=losses)
            with textcat.model.use_params(optimizer.averages):
                # evaluate on the dev data split off in load_data()
                scores = evaluate(nlp.tokenizer, textcat, test_texts,
                                  test_cats)
            print("{0:.3f}\t{1:.3f}\t{2:.3f}\t{3:.3f}".
                  format(  # print a simple table
                      losses["textcat"],
                      scores["textcat_p"],
                      scores["textcat_r"],
                      scores["textcat_f"],
                  ))
            mlflow.log_metric("loss", losses["textcat"])
            mlflow.log_metric("precision", scores["textcat_p"])
            mlflow.log_metric("recall", scores["textcat_r"])
            mlflow.log_metric("f1", scores["textcat_f"])

            if scores['textcat_f'] > best_f:
                nlp.to_disk(tmp_dir)
                best_f = scores['textcat_f']

    nlp = spacy.load(tmp_dir)
    shutil.rmtree(tmp_dir)

    if output_dir is not None:
        nlp.to_disk(output_dir)
        print("Saved model to", output_dir)

    docs = nlp.pipe(test_texts)
    cats = []
    for doc in docs:
        cats.append(max(doc.cats.items(), key=lambda x: x[1])[0])

    return cats


def evaluate(tokenizer, textcat, texts, cats):
    docs = (tokenizer(text) for text in texts)
    tp = 0.0  # True positives
    fp = 1e-8  # False positives
    fn = 1e-8  # False negatives
    tn = 0.0  # True negatives
    for i, doc in enumerate(textcat.pipe(docs)):
        gold = cats[i]
        for label, score in doc.cats.items():
            if label not in gold:
                continue
            if label == "NEGATIVE":
                continue
            if score >= 0.5 and gold[label] >= 0.5:
                tp += 1.0
            elif score >= 0.5 and gold[label] < 0.5:
                fp += 1.0
            elif score < 0.5 and gold[label] < 0.5:
                tn += 1
            elif score < 0.5 and gold[label] >= 0.5:
                fn += 1
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    if (precision + recall) == 0:
        f_score = 0.0
    else:
        f_score = 2 * (precision * recall) / (precision + recall)
    return {"textcat_p": precision, "textcat_r": recall, "textcat_f": f_score}
