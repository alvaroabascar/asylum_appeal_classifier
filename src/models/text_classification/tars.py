"""
Try a zero-shot classifier. We use TARS from FLAIR:
See https://github.com/flairNLP/flair/blob/master/resources/docs/
TUTORIAL_10_TRAINING_ZERO_SHOT_MODEL.md
"""
import os
import pathlib

import tqdm
from flair.data import Corpus, Sentence
from flair.datasets import SentenceDataset
from flair.models.text_classification_model import TARSClassifier
from flair.trainers import ModelTrainer

CURDIR = pathlib.Path(os.path.dirname(__file__))
MODELS_PATH = pathlib.Path(CURDIR, '../../../models')
TARS_BASE_PATH = MODELS_PATH.joinpath('tars_asylum')
TARS_BASE_PICKLE_PATH = TARS_BASE_PATH.joinpath('final-model.pt')
TARS_FINAL_PATH = MODELS_PATH.joinpath('tars_asylum_type_a_type_b')

LABEL_NAME_MAP = {
    'RETURN_TO_TRIAL':
    'appellant will return to trial',
    'DISMISSED':
    'appeal is dismissed',
    'APPROVED':
    'appeal is approved',
    'TYPE_A':
    'citizen is the appellant',
    'TYPE_B':
    'home office is the appellant',
    'REVERSE_LANGUAGE': ('appeal is done by home office but we '
                         'refer as appellant to the citizen')
}

# LABEL_NAME_MAP = {
#     'RETURN_TO_TRIAL': 'appellant will go back to first tier tribunal',
#     'DISMISSED': 'the request of the apellant was rejectd',
#     'APPROVED': 'the request of the appellant was accepted',
#     'TYPE_A': 'home office is the respondent',
#     'TYPE_B': 'citizen is the respondent',
#     'REVERSE_LANGUAGE': ('we name the citizen as appellant despite the home '
#                          'office is the real appellant')
# }

REVERSE_LABEL_NAME_MAP = {b: a for a, b in LABEL_NAME_MAP.items()}


def predict_tars_zero(dataset: dict):
    # texts_train = [e['text'] for e in dataset['train']]
    # labels_train = [e['label_names'][0] for e in dataset['train']]
    texts_valid = [e['text'] for e in dataset['validation']]
    labels_valid = [e['label_names'][0] for e in dataset['validation']]
    labels_valid = [LABEL_NAME_MAP[l] for l in labels_valid]

    # 1. Load our pre-trained TARS model for English
    tars = TARSClassifier.load('tars-base')

    # 2. Prepare a sentences
    sentences_valid = [Sentence(text) for text in texts_valid]
    classes = list(set(labels_valid))

    preds_valid = []
    for sentence in tqdm.tqdm(sentences_valid):
        tars.predict_zero_shot(sentence, classes)
        pred = list(
            sorted(((label.score, label.value) for label in sentence.labels),
                   reverse=True))
        if not pred:
            pred = 'UNK'
        else:
            pred = pred[0][1]
        label = REVERSE_LABEL_NAME_MAP.get(pred, 'UNK')
        print('pred:', pred)
        print('label:', label)
        preds_valid.append(label)

    return preds_valid


def train_tars(dataset: dict):
    corpus_name = 'asylum-type-a-type-b'
    label_name = 'asylum-type-a-type-b'

    texts_train = [e['text'] for e in dataset['train']]
    labels_train = [
        LABEL_NAME_MAP[e['label_names'][0]] for e in dataset['train']
    ]
    texts_valid = [e['text'] for e in dataset['validation']]
    labels_valid = [
        LABEL_NAME_MAP[e['label_names'][0]] for e in dataset['validation']
    ]

    train = SentenceDataset([
        Sentence(text).add_label(label_name, label)
        for text, label in zip(texts_train, labels_train)
    ])
    test = SentenceDataset([
        Sentence(text)  # .add_label(label_name, label)
        for text, label in zip(texts_valid, labels_valid)
    ])

    corpus = Corpus(train=train, test=test)

    tars = TARSClassifier.load(TARS_BASE_PICKLE_PATH)

    tars.add_and_switch_to_new_task(
        corpus_name, label_dictionary=corpus.make_label_dictionary())

    trainer = ModelTrainer(tars, corpus)

    trainer.train(
        base_path=TARS_FINAL_PATH,
        learning_rate=0.02,  # use very small learning rate
        mini_batch_size=1,  # small mini-batch size since corpus is tiny
        max_epochs=10,  # terminate after 10 epochs
        train_with_dev=True)
    tars = TARSClassifier.load(TARS_FINAL_PATH)

    preds_valid = []
    for sentence in tqdm.tqdm(test):
        tars.predict(sentence)
        pred = list(
            sorted(((label.score, label.value) for label in sentence.labels),
                   reverse=True))
        if not pred:
            pred = 'UNK'
        else:
            pred = pred[0][1]
        label = REVERSE_LABEL_NAME_MAP.get(pred, 'UNK')
        preds_valid.append(label)

    return preds_valid
