from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.0.1',
    description='Project to automatically classify asylum appeals into approved, denied or submitted for new trial.',
    author='Álvaro Abella',
    license='MIT',
)
