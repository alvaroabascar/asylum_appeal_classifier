### Data files

#### Overview

1. Court appeals are scraped from Bailii and tribunaldecisions.
2. Data is unified into a single file where all entries share a common format.
3. Data is formatted in a prodigy-compatible way.
4. Data is labeled using prodigy.

#### Files

1. `data/interim/unreported_cases_bailii.json`:

Produced by `dvc_files/unreported_cases_bailii.dvc`. Data is scraped
using `src/data/spiders/unreported_cases_bailii.py`. These are unreported
cases until 1st June 2013.

2. `data/interim/reported_unreported_tribunaldecisions.json`:

Produced by `dvc_files/reported_unreported_tribunaldecisions.dvc`. Data is
scraped using `src/data/spiders/reported_unreported_cases_tribunaldecisions.py`.

3. `data/interim/unified_data.json`

Produced by `dvc_files/unify_data.dvc`. The python script which performs this
homogenization is `src/data/unify_bailii_tribunaldecisions.py`. Each appeal
has a "reference_number" which will later be used as "label" (in the sense of
"identifier").

4. `data/interim/data_to_classify.jsonl`

Produced by `dvc_files/data_to_prodigy.dvc`, using the script
`src/data/data_to_prodigy.py`. This json contains a random sample of 2000
appeals, stratified by year. It is ready to be classified using prodigy.

5. `data/interim/data_classified.jsonl`

Data already labeled using prodigy. It will be a subsample of
`data/interim/data_to_classify.jsonl`, where the `accept` and `answer` entries
have been set.

6. `data/processed/database.sqlite`

SQLite containing all cases in a sql schema. Buitlt by `src/data/create_sqlite.py`.

#### Process

1. Scrap data
2. Build a database